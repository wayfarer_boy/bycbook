'use strict';

// Posts controller
angular.module('posts')
/*.factory('mySocket', function (socketFactory) {*/
/*return socketFactory();*/
/*})*/
  .controller('PostsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Posts', 'Files', 
    function($scope, $stateParams, $location, Authentication, Posts, Files) {
      $scope.authentication = Authentication;
      $scope.selectedTag = $stateParams.hashtag;

      // Create new Post
      $scope.create = function() {
        if (this.tweet && this.tweet.length > 140) {
          $scope.error = 'Text is over 140 characters.';
        } else {
          // Create new Post object
          var fileId = this.file ? this.file._id : null;
          if (fileId) {
            var post = new Posts({
              tweet: this.tweet,
              file: fileId
            });

            // Redirect after save
            post.$save(function(response) {
              $location.path('posts');
              /*mySocket.emit('new:post');*/
            }, function(errorResponse) {
              $scope.error = errorResponse.data.message;
            });

              // Clear form fields
            this.title = '';
            this.file = null;
          } else {
            $scope.error = 'You need to upload a file';
          }
        }
      };

      // Remove existing Post
      $scope.remove = function(post) {
        if (post) {
          var file = new Files(post.file);
          file.$remove();
          post.$remove();
          for (var i in $scope.posts) {
            if ($scope.posts[i] === post) {
              $scope.posts.splice(i, 1);
            }
          }
        } else {
          $scope.post.$remove(function() {
            $location.path('posts');
          });
        }
        /*mySocket.emit('remove:post', post);*/
      };

      // Update existing Post
      $scope.update = function() {
        var post = $scope.post;
        post.$update(function() {
          $location.path('posts/' + post._id);
          /*mySocket.emit('update:post', post);*/
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      // Find a list of Posts
      $scope.find = function() {
        $scope.posts = Posts.query();
        /*if (Authentication.user.roles.indexOf('admin') > -1 && false) {*/
          /*mySocket.on('new:post', $scope.remoteUpdate('new'));*/
          /*mySocket.on('remove:post', $scope.remoteUpdate('remove'));*/
          /*mySocket.on('update:post', $scope.remoteUpdate('update'));*/
        /*}*/
      };

      /* // Another user has updated the posts
      $scope.remoteUpdate = function (action) {
        return function (data) {
          if (data.id !== mySocket.id) {
            if (action === 'new') {
              $scope.find();
            } else {
              for (var i in $scope.posts) {
                if ($scope.posts[i]._id === data.post._id) {
                  if (action === 'update') {
                    $scope.posts[i] = data.post;
                  } else {
                    $scope.posts.splice(i,1);
                  }
                  break;
                }
              }
            }
          }
        };
      }; */

      // Find existing Post
      $scope.findOne = function() {
        $scope.post = Posts.get({
          postId: $stateParams.postId
        });
      };

      $scope.fileRemove = function () {
        var post = $scope.post;
        var file = new Files(post.file);
        file.$remove();
      };

      $scope.expandPost = function (index, expand) {
        $scope.posts[index].expanded = expand;
      };

      $scope.remainingLength = function (tweet) {
        if (tweet) {
          return 140 - tweet.length;
        }
        return 140;
      };

      $scope.checkChars = function (tweet) {
        var valid = true;
        if (tweet) {
          valid = tweet.length > 140;
          if (valid) {
            $scope.error = '';
          }
        }
        return valid;
      };
    }
  ]);
