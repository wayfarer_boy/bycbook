'use strict';

//Hashtags service used to communicate Hashtags REST endpoints
angular.module('hashtags').factory('Hashtags', ['$resource', function($resource) {
    return $resource('hashtags/:hashtagId', {
        hashtagId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);