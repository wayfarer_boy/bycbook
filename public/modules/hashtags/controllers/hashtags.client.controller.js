'use strict';

// Hashtags controller
angular.module('hashtags')
/*.factory('mySocket', function (socketFactory) {*/
/*return socketFactory();*/
/*})*/
  .controller('HashtagsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Hashtags',
    function($scope, $stateParams, $location, Authentication, Hashtags) {
      $scope.authentication = Authentication;
      $scope.selectedTag = $stateParams.hashtag;

      // Create new Hashtag
      $scope.create = function() {
        // Create new Hashtag object
        var hashtag = new Hashtags({
          tag: this.tag,
          description: this.description,
          campaign: this.campaign
        });

        // Redirect after save
        hashtag.$save(function(response) {
          /*mySocket.emit('new:hashtag');*/
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });

        // Clear form fields
        this.tag = '';
        this.description = '';
        this.campaign = '';
      };

      // Remove existing Hashtag
      $scope.remove = function(hashtag) {
        if (hashtag) {
          hashtag.$remove();

          for (var i in $scope.hashtags) {
            if ($scope.hashtags[i] === hashtag) {
              $scope.hashtags.splice(i, 1);
            }
          }
        } else {
          $scope.hashtag.$remove(function() {
            $location.path('hashtags');
          });
        }
        /*mySocket.emit('remove:hashtag', hashtag);*/
      };

      // Update existing Hashtag
      $scope.update = function() {
        var hashtag = $scope.hashtag;

        hashtag.$update(function() {
          $location.path('hashtags');
          /*mySocket.emit('update:hashtag', hashtag);*/
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      /*// Another user has updated the posts
      $scope.remoteUpdate = function (action) {
        return function (data) {
          if (data.id !== mySocket.id) {
            if (action === 'new') {
              $scope.find();
            } else {
              for (var i in $scope.hashtags) {
                if ($scope.hashtags[i]._id === data.hashtag._id) {
                  if (action === 'update') {
                    $scope.hashtags[i] = data.hashtag;
                  } else {
                    $scope.hashtags.splice(i,1);
                  }
                  break;
                }
              }
            }
          }
        };
      };*/

      // Find a list of Hashtags
      $scope.find = function() {
        Hashtags.query(function (data) {
          $scope.hashtags = data;
          $scope.campaigns = [];
          var cList = {};
          for (var i in $scope.hashtags) {
            var tag = $scope.hashtags[i];
            if (tag.tag) {
              console.log(tag);
              if (!cList[tag.campaign]) {
                $scope.campaigns.push({
                  campaign: tag.campaign,
                  tags: []
                });
                cList[tag.campaign] = { index: $scope.campaigns.length - 1 };
              }
              $scope.campaigns[cList[tag.campaign].index].tags.push(tag);
            }
          }
          console.log($scope.campaigns);
        });
        /*if (Authentication.user.roles.indexOf('admin') > -1 && false) {*/
        /*mySocket.on('new:hashtag', $scope.remoteUpdate('new'));*/
        /*mySocket.on('remove:hashtag', $scope.remoteUpdate('remove'));*/
        /*mySocket.on('update:hashtag', $scope.remoteUpdate('update'));*/
        /*}*/
      };

      // Find existing Hashtag
      $scope.findOne = function() {
        $scope.hashtag = Hashtags.get({
          hashtagId: $stateParams.hashtagId
        });
      };
    }
  ]);
