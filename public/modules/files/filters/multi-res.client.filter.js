'use strict';

angular.module('files').filter('multiRes', [
	function() {
		return function(input) {
      var size = input.split('x');
      var sizeDouble = [size[0]*2 || '', size[1]*2 || ''];
      return sizeDouble.join('x');
		};
	}
]);
