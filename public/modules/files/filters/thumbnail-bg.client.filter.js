'use strict';

angular.module('files').filter('thumbnailBg', [
	function() {
		return function(input) {
			// Thumbnail bg directive logic 
			// ...

      if (input.type === 'image') {
        return input.path;
      }
      return input.path+'.jpg';
		};
	}
]);
