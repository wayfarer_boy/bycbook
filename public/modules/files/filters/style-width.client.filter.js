'use strict';

angular.module('files').filter('styleWidth', [
	function() {
		return function(input) {
			return 'width: ' + input + '%;';
		};
	}
]);
