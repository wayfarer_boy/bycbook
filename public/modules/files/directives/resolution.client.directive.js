'use strict';

angular.module('files').directive('resolution', [
	function() {
		return {
			restrict: 'A',
			link: function postLink(scope, element, attrs) {	
        var size = attrs.resolution.split('x');
        var sizeDouble = [size[0]*2 || '', size[1]*2 || ''];
        scope.size = {
          lores: size.join('x'),
          hires: sizeDouble.join('x')
        };
			}
		};
	}
]);
