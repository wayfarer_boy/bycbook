'use strict';

angular.module('files').directive('uploader', [
	function() {
		return {
      templateUrl: 'modules/files/views/uploader.html',
			restrict: 'E',
      scope: {
        file: '=',
        resolution: '@',
        choose: '@',
        replace: '@',
        extensions: '@'
      },
			link: function postLink(scope, element, attrs) {
        var button = element.find('button.uploader-click');
        var input = element.find('input.file-selector');
        button.click(function (e) {
          e.preventDefault();
          input.click();
        });
			},
      controller: ['$scope', '$upload', 'Files', function ($scope, $upload, Files) {
        $scope.options = {
          resolution: $scope.resolution || '320x',
          choose: $scope.choose || 'Choose a file',
          replace: $scope.replace || 'Replace file',
          extensions: $scope.extensions.toLowerCase() || 'jpg,png,gif,jpeg'.toLowerCase()
        };
        $scope.options.extensions = $scope.options.extensions.split(',');
        $scope.ext_re = /(?:\.([^.]+))?$/;

        $scope.onFileSelect = function ($file) {
          if ($file[0]) {
            var ext = $scope.ext_re.exec($file[0].name)[1];
            var match = $scope.options.extensions.indexOf(ext.toLowerCase());
            if (match > -1) {
              
              // Cancel any previous uploads
              if ($scope.upload && $scope.upload !== null) {
                $scope.upload.abort();
              }
              if ($scope.file) {
                var file = new Files($scope.file);
                file.$remove();
                $scope.file = null;
              }
              $scope.error = false;
              $scope.selectedFile = $file[0];
              $scope.progress = 0;
              $scope.upload = $upload.upload({
                url: 'files/upload',
                file: $scope.selectedFile,
                fileFormDataName: 'myFile'
              }).then(function (response) {
                if (response.data && response.data.message) {
                  $scope.error = response.data.message;
                } else {
                  $scope.file = response.data.file;
                }
                $scope.progress = false;
                $scope.selectedFile = null;
              }, null, function (evt) {
                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                if ($scope.progress === 100) {
                  $scope.progress = false;
                }
              });
            } else {
              $scope.error = 'Only the following extensions are allowed: '+$scope.options.extensions.join(', ');
            }
          }
        };

      }]
		};
	}
]);
