'use strict';

angular.module('core').controller('FooterController', ['$scope', '$http',
	function($scope, $http) {
    $http.get('/info').success(function(data) {
      angular.extend($scope, data);
    });
	}
]);
