'use strict';

/**
 * Module dependencies
 */

var http = require('http');
var socketio = require('socket.io');
var _ = require('lodash');

module.exports = function (app) {
  var server = http.createServer(app);
  var io = socketio.listen(server);

  var socketListen = function (socket, action, type, oldArr) {
    var arr = _.cloneDeep(oldArr);
    socket.on(action+':'+type, function (data) {
      if (action !== 'new') {
        arr[type] = data;
      }
      io.sockets.emit(action+':'+type, arr);
    });
  };

  io.on('connection', function (socket) {
    var id = socket.id;
    var arr = { id: id };
    var actions = ['new', 'remove', 'update'];
    var types = ['post', 'hashtag'];
    for (var type in types) {
      for (var action in actions) {
        socketListen(socket, actions[action], types[type], arr);
      }
    }

  });

  return server;
};

