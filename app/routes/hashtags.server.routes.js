'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var hashtags = require('../../app/controllers/hashtags');

	// Hashtags Routes
	app.route('/hashtags')
		.get(hashtags.list)
		.post(users.requiresLogin, hashtags.create);
	
	app.route('/hashtags/:hashtagId')
		.get(hashtags.read)
		.put(users.requiresLogin, hashtags.hasAuthorization, hashtags.update)
	    .delete(users.requiresLogin, hashtags.hasAuthorization, hashtags.delete);

	// Finish by binding the Hashtag middleware
	app.param('hashtagId', hashtags.hashtagByID);
};