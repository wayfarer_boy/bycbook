'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var files = require('../../app/controllers/files');

	// Files Routes
	app.route('/files')
		.get(files.list)
		.post(users.requiresLogin, files.create);
	
  app.route('/files/upload')
    .post(users.requiresLogin, files.upload);
	
	app.route('/files/:fileId')
		.get(files.read)
		.put(users.requiresLogin, files.update)
	    .delete(users.requiresLogin, files.delete);

	// Finish by binding the File middleware
	app.param('fileId', files.fileByID);
};
