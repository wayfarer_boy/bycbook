'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var _ = require('lodash');
var interfaceAddresses = require('interface-addresses');
var addresses = interfaceAddresses();
var ip = addresses.eth0 || addresses.wlan0 || null;
if (ip) {
  var util  = require('util');
  var exec = require('child_process').exec;
  var child = exec('qrencode -o '+[__dirname, '..', '..', 'public', 'uploads', 'site.png'].join('/')+' http://'+ip+':3000', function (err, stdout, stderr) {
    if (!err) {
      console.log('site qrcode created');
    }
  });
}

/**
 * Show the current Site info
 */
exports.read = function(req, res) {
  res.send({
    site: {
      ip: ip
    }
  });
};

